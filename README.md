# CLI Boilerplate

## Installation

Pour l'installation :

1. Faire un clone du projet
1. Supprimé le dossier .git
    1. Lancer la commande ```git init```
    1. Appliquer le nouveau repo ``git remote add origin URL_REPO``
1. Modifier le fichier ``CHANGELOG.md`` pour avoir le bon nom d'application

## Developpement

1. Backend de l'application
    1. Lancer les commandes : ``npm run watch:backend`` et ``npm run run:backend``

## Verification

Lancer la commande : ``npm run ci``

Vérification effectué : TypeScript Type checking, Linting (Script) et Prettier

## Test

### Test Unitaire

Lancer la commande : ``npm run test:unit``
Si on veut watch : ``npm run test:unit:watch``

## Release

### Creation d'une version

Lancer la commande : ``npm run release``

Suivre les étapes a l'écran. Déplacé les fichiers produits dans le dossier .dist a l'endroit approprié pour la MAJ automatique ou les tests usagés.

## Structure

### Dossier

**application** : Le dossier application contient la logique métier de l'application.

**common** : Le dossier common contient les classes communes de l'application. Le code source qui sera utilisé dans les workers, backend et frontend de l'application.

**thread** : Le dossier thread contient les logiques pour créer les workers de l'application. Le dossier n'aura pas besoin de modification en général.

**worker** : Le dossier worker contient la logique métier de l'application pour les jobs extensives qui doivent être executés par un autre processus.
