const path = require('path');
const nodeExternals = require('webpack-node-externals');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');

module.exports = {
  target: 'node',
  module: {
    rules: [
      /* config.module.rule('ts') */
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
            },
          },
        ],
      },
      /* config.module.rule('js') */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [['@babel/preset-env', { targets: 'defaults' }]],
          },
        },
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js', '.json'],
    alias: {
      '@common': path.resolve(__dirname, '../src/common'),
      '@root': path.resolve(__dirname, '..'),
      '@src': path.resolve(__dirname, '../src'),
    },
  },
  devtool: 'cheap-module-source-map',
  externals: [nodeExternals()],
  optimization: {
    runtimeChunk: 'single',
    minimize: false,
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: 20,
      minSize: 100000,
      cacheGroups: {
        vendor: {
          test: /[/\\]node_modules[/\\]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            const packageName = module.context.match(/[/\\]node_modules[/\\](.*?)([/\\]|$)/)[1];

            // npm package names are URL-safe, but some servers don't like @ symbols
            return `vendor-${packageName.replace('@', '')}`;
          },
        },
      },
    },
  },
  stats: {
    all: false,
    env: true,
    outputPath: true,
    publicPath: true,
    assets: true,
    entrypoints: true,
    chunkGroups: true,
    chunks: true,
    modules: true,
    warnings: true,
    errors: true,
    errorDetails: true,
    errorStack: true,
    moduleTrace: true,
    builtAt: true,
    errorsCount: true,
    warningsCount: true,
    timings: true,
    version: true,
    hash: true,
  },
  plugins: [new CaseSensitivePathsPlugin(), new FriendlyErrorsWebpackPlugin()],
};
