const spawn = require('cross-spawn');
const child = require('child_process');
const fs = require('fs');
const path = require('path');
const prompts = require('prompts');
const packageFile = require('../package.json');

let version;

function getNextVersion(type) {
  let currentVersion = packageFile.version;
  let [major, minor, patch] = currentVersion.split('.');
  switch (type) {
    case 0:
      patch++;
      break;
    case 1:
      minor++;
      break;
    case 2:
      major++;
      break;
  }

  return [major, minor, patch].join('.');
}

let questions = [
  {
    type: 'select',
    name: 'type',
    message: 'Choisir le type de version :',
    choices: [
      { title: 'Patch', value: 0 },
      { title: 'Minor', value: 1 },
      { title: 'Major', value: 2 },
      { title: 'Custom', value: 3 },
    ],
    initial: 0,
  },
  {
    type: (prev) => (prev === 3 ? 'text' : null),
    name: 'version',
    message: `Entrer le numéro de la version :`,
    validate: (value) => (/(?:\d)+\.(?:\d)+\.(?:\d)+/.test(value) ? true : 'La version doit-être en format [X.X.X]'),
  },
  {
    type: 'confirm',
    name: 'confirmation',
    message: (prev, values) => {
      if (values.type === 3) {
        version = values.version;
      } else {
        version = getNextVersion(values.type);
      }

      return `La prochaine version sera ${version}`;
    },
    initial: true,
  },
];

prompts(questions)
  .then((response) => {
    if (!response.confirmation) {
      console.error("La version a été annulé par l'utilisateur.");
      return;
    }

    packageFile.version = version;
    fs.writeFileSync(path.resolve(process.cwd(), 'package.json'), JSON.stringify(packageFile, null, '  '));
    spawn.sync('git', ['add', '.'], { stdio: 'inherit' });
    spawn.sync('git', ['commit', '-m', 'Mise a jour de la version.'], { stdio: 'inherit' });
    spawn.sync('npm', ['version', version], { stdio: 'inherit' });

    child.execSync('build.sh', { stdio: 'inherit' });

    spawn.sync('git', ['push', '--tags'], { stdio: 'inherit' });
  })
  .catch((issue) => {
    console.error(issue);
  });
