import 'reflect-metadata';
import 'core-js';

import './ioc';
import { program } from '@caporal/core';
import { container } from 'tsyringe';
import packages from '../package.json';
import { CliInformation } from '@common/interfaces/cli-information';
import { setStarterSetting } from '@common/helpers/starter-setting';
import { Main } from '@src/application/controllers/main';

let settings: CliInformation;

program
  .name(packages.name)
  .version(packages.version)
  .strict(false)
  .argument('<env>', `Mode de l'application`, { default: 'PROD', validator: /PROD|DEV|LOCAL/ })
  .action(async ({ args }) => {
    settings.path = __dirname;
    settings.env = 'LOCAL';

    container.register('info', { useValue: settings });
    return new Main().start(args);
  });

(async () => {
  settings = setStarterSetting();
  await program.run();
})();
