export interface CliInformation {
  level: string;
  debug: boolean;
  env: string;
  path: string;
}
