import fs from 'fs';
import path from 'path';
import walkBack from 'walk-back';
import { DiskInformation } from '@common/interfaces/disk-information';

export function getDriveFromEnvironment(environment: string): DiskInformation {
  switch (environment.toUpperCase()) {
    case 'LOCAL':
      return { config: 'C:', data: 'U:', stockage: 'V:' };
    case 'DEV':
      return { config: 'U:', data: 'U:', stockage: 'V:' };
    case 'PROD':
      return { config: 'M:', data: 'M:', stockage: 'S:' };
    default:
      return { config: 'C:', data: 'U:', stockage: 'V:' };
  }
}

export function getScriptPath(target: string, appPath: string): string {
  // Walk down the tree to find worker.bundle.js if it's not in the out directories.
  const outDirectory = path.resolve(appPath, '.out');
  const startAt = fs.existsSync(outDirectory) ? outDirectory : appPath;
  return walkBack(startAt, target);
}

export function convertMapToPlain<T>(map: Map<string, T>): Record<string, unknown> {
  // eslint-disable-next-line unicorn/no-reduce
  const flatObject = [...map].reduce(
    (sourceObject, [key, value]) => Object.assign(sourceObject, { [key]: value }), // Be careful! Maps can have non-String keys; object literals can't.
    {},
  );
  return flatObject;
}
