import parser from 'yargs-parser';
import { CliInformation } from '@common/interfaces/cli-information';

export function setStarterSetting(): CliInformation {
  let level = '0';

  // Activate verbose mode
  const argv = parser(process.argv);

  if (argv.quiet) {
    level = '4';
  }

  if (argv.v || argv.verbose) {
    level = '7';
  }

  let debug = false;
  if (argv.debug) {
    debug = argv.debug;
  }

  return {
    level,
    debug,
    env: '',
    path: '',
  };
}
