import Conf from 'conf';
import { registry } from 'tsyringe';
import { InjectFromContainer } from 'di-manager';
import packageJSON from '@root/package.json';
import { PathUtility } from '@common/services/path-utility';
import { Configuration } from '@common/models/configuration';
import { CliInformation } from '@common/interfaces/cli-information';
import { getDriveFromEnvironment } from '@common/helpers/utility';

@registry([{ token: 'ConfigurationService', useValue: new ConfigurationService() }])
export class ConfigurationService {
  @InjectFromContainer('PathUtility')
  public pathUtility!: PathUtility;

  public configuration!: Configuration;

  private readonly userConfig!: Conf;

  constructor() {
    this.userConfig = new Conf();
  }

  public getUserConfig(): Conf {
    return this.userConfig;
  }

  public getValue<T>(key: string): T {
    return this.userConfig.get(key) as T;
  }

  public setValue<T>(key: string, value: T): void {
    this.userConfig.set(key, value);
  }

  public setInformation(info: CliInformation): void {
    this.configuration = Configuration.fromJSON({
      env: info.env,
      disk: getDriveFromEnvironment(info.env),
      version: packageJSON.version,
    });

    this.pathUtility.setCurrentDrive(this.configuration.disk);
  }
}
