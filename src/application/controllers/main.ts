import { container } from 'tsyringe';
import { InjectFromContainer } from 'di-manager';
import { ParsedArgumentsObject } from '@caporal/core';
import { CliInformation } from '@common/interfaces/cli-information';
import { ConfigurationService } from '@common/services/configuration';

export class Main {
  @InjectFromContainer('ConfigurationService')
  private readonly configurationService!: ConfigurationService;

  constructor() {
    this.configurationService.setInformation(container.resolve<CliInformation>('info'));
  }

  public start(args: ParsedArgumentsObject): number {
    console.log(args, this.configurationService.configuration);
    return 0;
  }
}
