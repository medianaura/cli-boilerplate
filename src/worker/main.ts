import 'reflect-metadata';
import 'core-js';

class WorkerProcessFactory {
  getActionFactory(cmd: string, options: any): any {
    switch (cmd.toLowerCase()) {
      case 'setup':
        return { cmd: 'setup', status: true };
      default:
        return { cmd: 'done', status: 0 };
    }
  }
}

const main = new WorkerProcessFactory();

process.on('message', (input) => {
  const output = main.getActionFactory(input.cmd, input.options);
  if (!output) {
    return;
  }

  // @ts-expect-error
  process.send(output);
});
