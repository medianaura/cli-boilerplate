import { Pool } from '@src/thread/pool';
import { Job } from '@src/thread/job';
import { getScriptPath } from '@common/helpers/utility';

const numberCPUs = require('os').cpus().length;

export class PoolHelper {
  private static instance: PoolHelper;

  private pool!: Pool;

  private constructor() {
    // Block constructor
  }

  public static getInstance(): PoolHelper {
    if (typeof this.instance === 'undefined') {
      this.instance = new PoolHelper();
    }

    return this.instance;
  }

  public createPool(options: string[] = [], threads: number = numberCPUs): void {
    if (typeof this.pool !== 'undefined') {
      this.pool.reset();
    }

    const scriptPath = getScriptPath('worker.js', __dirname);
    this.pool = new Pool(scriptPath, threads, options);
  }

  public addJob(job: Job): void {
    this.pool.addJob(job);
  }

  public setupWorker(options: any = {}): void {
    this.pool.setupWorker({ cmd: 'setup', options });
  }

  public bindSetup(callback: (m: any) => void): void {
    this.pool.on('setup', callback);
  }

  public bindSetupError(callback: (m: any) => void): void {
    this.pool.on('setup_error', callback);
  }

  public bindProgress(callback: (m: any) => void): void {
    this.pool.on('progress', callback);
  }

  public bindDone(callback: (m: any) => void): void {
    this.pool.on('done', callback);
  }

  public bindFinished(callback: (m: any) => void): void {
    this.pool.on('finished', callback);
  }
}
